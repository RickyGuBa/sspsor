import React, { Component } from 'react';
// Styles
import styles from "./Tarea07.module.css";
// Components
import TitleComponent from '../../Components/TitleComponent/TitleComponent';
import SOSelector from '../../Components/SOSelector/SOSelector';
// Icons
import { IoLogoWindows, IoLogoTux, IoLogoApple } from "react-icons/io";
// Images
import Windows1 from "./img/Windows1.png";
import Linux1 from "./img/Linux1.png";
import Mac1 from "./img/Mac1.png";

class Tarea07 extends Component {

    constructor(props) {
        super(props);
        this.state = {
            OS: "Windows",
            systems: [
                {name: "Windows", icon: IoLogoWindows},
                {name: "Linux", icon: IoLogoTux},
                {name: "MacOS", icon: IoLogoApple},
            ]
        }
    }

    handleOnChange = (OS) => {
        this.setState({
            OS
        })
    }

    renderImages = () => {
        switch (this.state.OS) {
            case "Windows":
                return <div className={styles.Image}><img src={Windows1} alt="" /></div>
            case "Linux":
                return <div className={styles.Image}><img src={Linux1} alt="" /></div>
            case "MacOS":
                return <div className={styles.Image}><img src={Mac1} alt="" /></div>
        
            default:
                break;
        }
    }

    render() {
        return (
            <div className={styles.Tarea}>
                <TitleComponent>Desarrollar un pequeño videojuego en Windows y Linux.</TitleComponent>
                <h2>Requisitos</h2>
                <li className={styles.Requisito}>Python 3</li>
                <li className={styles.Requisito}>Librería Pygame</li>
                <li className={styles.Requisito}><a target="_blank" rel="noopener noreferrer" href="http://programarcadegames.com/python_examples/show_file.php?lang=es&file=platform_moving.py">Código fuente</a></li>
                <SOSelector systems={this.state.systems} selected={this.state.OS} onChange={this.handleOnChange}/>
                {this.renderImages()}
            </div>
        );
    }
}

export default Tarea07;