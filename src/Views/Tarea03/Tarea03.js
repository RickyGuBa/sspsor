import React, { Component } from 'react';
// Styles
import styles from "./Tarea03.module.css";
// Components
import TitleComponent from '../../Components/TitleComponent/TitleComponent';
import SOSelector from '../../Components/SOSelector/SOSelector';
// Icons
import { IoLogoWindows, IoLogoTux } from "react-icons/io";
// Images
import Windows1 from "./img/Windows1.png";
import Windows2 from "./img/Windows2.png";
import Windows3 from "./img/Windows3.png";
import Windows4 from "./img/Windows4.png";
import Windows5 from "./img/Windows5.png";
import Linux1 from "./img/Linux1.png";
import Linux2 from "./img/Linux2.png";
import Linux3 from "./img/Linux3.png";
import Linux4 from "./img/Linux4.png";
import Linux5 from "./img/Linux5.png";


const Windows = () => {
    return (
        <div>
            <h3>
                Entramos al sitio oficial de <a target="_blank" rel="noopener noreferrer" href="https://es.libreoffice.org/descarga/libreoffice/">LibreOffice</a> y descargamos el archivo correspondiente a la de nuestro sistema operativo.
            </h3>
            <div className={styles.Image}><img src={Windows1} alt="" /></div>
            <h3>Una vez que se terminó la descarga, procedemos a ejecutar el archivo descargado.</h3>
            <div className={styles.Image}><img src={Windows2} alt="" /></div>
            <h3>
                Se pasa por una serie de pasos para configurar donde seleccionamos el tipo de instalación "Típica" y creamos accesos directos, 
                una vez configurado esto, el instalador comenzará con el proceso.
            </h3>
            <div className={styles.Image}><img src={Windows3} alt="" /></div>
            <h3>El inststalador nos avisará cuando el programa haya quedado instalado y solicitará un reinicio del equipo.</h3>
            <div className={styles.Image}><img src={Windows4} alt="" /></div>
            <h3>Una vez que se completó el reinicio, podemos ejecutar LibreOffice desde los accesos directos y comenzar a usarlo.</h3>
            <div className={styles.Image}><img src={Windows5} alt="" /></div>
        </div>
    );
};

const Linux = () => {
    return (
        <div>
            <h3>
                Esta instalación se realizará mediante la Terminal de Linux, primero ingresamos el siguiente comando para 
                agregar el repositorio desde el que se descargará LibreOffice.
            </h3>
            <div className={styles.Codigo}><div className={styles.LineaCodigo}>sudo add-apt-repository ppa:libreoffice/ppa</div></div>
            <div className={styles.Image}><img src={Linux1} alt="" /></div>
            <h3>Una vez que se terminó de agregar el repositorio, ingresamos el siguiente comando para descargar el LibreOffice</h3>
            <div className={styles.Codigo}><div className={styles.LineaCodigo}>sudo apt install libreoffice</div></div>
            <div className={styles.Image}><img src={Linux2} alt="" /></div>
            <h3>
                Al inicio preguntará si se quiere instalar los paquetes intalados, y escribimos S para aceptar. Se comenzará a descargar 
                e instalar todo lo necesario, y se mostrará el progreso en la misma terminal.
            </h3>
            <div className={styles.Image}><img src={Linux3} alt="" /></div>
            <h3>
                Ya que se indicó que se instaló correctamente el LibreOffice, podemos ingresar el siguiente comando para abrir
                LibreOffice Writer.
            </h3>
            <div className={styles.Codigo}><div className={styles.LineaCodigo}>libreoffice --writer</div></div>
            <div className={styles.Image}><img src={Linux4} alt="" /></div>
            <h3>El programa comienza su ejecución y está completamente listo para su uso.</h3>
            <div className={styles.Image}><img src={Linux5} alt="" /></div>
        </div>
    );
};

class Tarea03 extends Component {

    constructor(props) {
        super(props);
        this.state = {
            OS: "Windows",
            systems: [
                {name: "Windows", icon: IoLogoWindows},
                {name: "Linux", icon: IoLogoTux},
            ]
        }
    }
    
    handleChangeOS = (OS) => {
        this.setState({
            OS
        })
    }

    renderOS = () => {
        switch (this.state.OS) {
            case "Windows":
                return <Windows />
            case "Linux":
                return <Linux />
            default:
                break;
        }
    }

    render() {
        return (
            <div className={styles.Tarea}>
                <TitleComponent>Instalar LibreOffice en Windows y en Linux.</TitleComponent>
                <SOSelector systems={this.state.systems} selected={this.state.OS} onChange={this.handleChangeOS}/>
                {this.renderOS()}
            </div>
        );
    }
}

export default Tarea03;