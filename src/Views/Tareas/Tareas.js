import React, { Component } from 'react';
// Ant Design
import { Select } from 'antd';
// Styles
import styles from "./Tareas.module.css";
// Icons
import { IoIosHome } from "react-icons/io";
// Tareas
import Tarea01 from '../Tarea01/Tarea01';
import Tarea02 from '../Tarea02/Tarea02';
import Tarea03 from '../Tarea03/Tarea03';
import Tarea04 from '../Tarea04/Tarea04';
import Tarea05 from '../Tarea05/Tarea05';
import Tarea06 from '../Tarea06/Tarea06';
import Tarea07 from '../Tarea07/Tarea07';
import Tarea08 from '../Tarea08/Tarea08';
import Tarea09 from '../Tarea09/Tarea09';
import Tarea10 from '../Tarea10/Tarea10';
import Tarea11 from '../Tarea11/Tarea11';
import Tarea12 from '../Tarea12/Tarea12';
import Tarea13 from '../Tarea13/Tarea13';

class Tareas extends Component {
    
    handleGoHome = () => {
        this.props.history.push("/");
    }

    getOptions = () => {
        const { Option } = Select;
        let options = []
        for(let i=1; i<=13; i++){
            options.push(
                <Option value={i} key={i}>Tarea {i}</Option>
            )
        }
        return options
    }

    handleOnChange = (value) => {
        this.props.history.push(`/tarea/${value}`)
    }

    getContent = () => {
        let tarea = Number(this.props.match.params.id);
        switch (tarea) {
            case 1:
                return <Tarea01 />
            case 2:
                return <Tarea02 />
            case 3:
                return <Tarea03 />
            case 4:
                return <Tarea04 />
            case 5:
                return <Tarea05 />
            case 6:
                return <Tarea06 />
            case 7:
                return <Tarea07 />
            case 8:
                return <Tarea08 />
            case 9:
                return <Tarea09 />
            case 10:
                return <Tarea10 />
            case 11:
                return <Tarea11 />
            case 12:
                return <Tarea12 />
            case 13:
                return <Tarea13 />
        
            default:
                return <h1>Not found</h1>
        }
    }
    
    render() {
        return (
            <div className={styles.Tareas}>
                <div className={styles.TareasHeader}>
                    <div className={styles.TareasHeaderLeft}>
                        <div className={styles.TareasHeaderLeftIcon} onClick={this.handleGoHome}><IoIosHome size={20}/></div>
                        <div className={styles.TareasHeaderLeftTitle}>Tarea {this.props.match.params.id}</div>
                    </div>
                    <div className={styles.TareasHeaderSelect}>
                        <Select defaultValue={Number(this.props.match.params.id)} style={{ width: "100%" }} onChange={this.handleOnChange} showSearch>
                            {this.getOptions()}
                        </Select>
                    </div>
                </div>
                <div className={styles.TareasContainer}>
                    {this.getContent()}
                </div>
            </div>
        );
    }
}

export default Tareas;