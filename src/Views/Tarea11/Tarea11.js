import React, { Component } from 'react';
// Styles
import styles from "./Tarea11.module.css";
// Components
import TitleComponent from '../../Components/TitleComponent/TitleComponent';
// Images
import Linux1 from "./img/Linux1.png";
import Linux2 from "./img/Linux2.png";
import Linux3 from "./img/Linux3.png";
// import Linux4 from "./img/Linux4.png";
import Linux5 from "./img/Linux5.png";
import Linux6 from "./img/Linux6.png";
import Linux7 from "./img/Linux7.png";
import Linux8 from "./img/Linux8.png";
import Linux9 from "./img/Linux9.png";
import Linux10 from "./img/Linux10.png";
import Linux11 from "./img/Linux11.png";
import Linux12 from "./img/Linux12.png";

class Tarea11 extends Component {
    render() {
        return (
            <div className={styles.Tarea}>
                <TitleComponent>
                    Instalar Linux Server y configurar 10 usuarios, configurar 4 grupos de usuarios. 
                </TitleComponent>
                <h2>Requisitos de instalación</h2>
                <li className={styles.Requisito}>Computadora</li>
                <li className={styles.Requisito}>USB Booteable</li>
                <li className={styles.Requisito}>ISO Ubuntu Server</li>
                <h2>Instalación</h2>
                <h3>Cargamos la USB con el ISO y procedemos a ejecurarla como en la instalación de Windows y Linux.</h3>
                <div className={styles.Image}><img src={Linux1} alt="" /></div>
                <h3>Seleccionamos el idioma que queremos.</h3>
                <div className={styles.Image}><img src={Linux2} alt="" /></div>
                <h3>Se nos guiará por distintas opciones de configuración para la instalación.</h3>
                <div className={styles.Image}><img src={Linux3} alt="" /></div>
                <h3>Una vez que terminamos con la configuración, se nos pedirá crear un usuario.</h3>
                <div className={styles.Image}><img src={Linux5} alt="" /></div>
                <h3>Después de crear el usuario, la instalación del Sistema Operativo comenzará.</h3>
                <div className={styles.Image}><img src={Linux6} alt="" /></div>
                <h3>Al finalizar, se mostrará la terminal de Ubuntu Server.</h3>
                <div className={styles.Image}><img src={Linux7} alt="" /></div>
                <h2>Creación de usuarios y grupos</h2>
                <h3>Para crear un usuario ejecutamos el siguiente comando y rellenamos los campos indicados.</h3>
                <div className={styles.Codigo}><div className={styles.LineaCodigo}>sudo adduser Nombre</div></div>
                <div className={styles.Image}><img src={Linux8} alt="" /></div>
                <h3>Una vez creados los 10 usuarios podemos comprobar mediante el siguiente comando.</h3>
                <div className={styles.Codigo}>
                    <div className={styles.LineaCodigo}>cd /home</div>
                    <div className={styles.LineaCodigo}>ls</div>
                </div>
                <div className={styles.Image}><img src={Linux9} alt="" /></div>
                <h3>Para crear un grupo ejecutamos el siguiente comando.</h3>
                <div className={styles.Codigo}><div className={styles.LineaCodigo}>sudo addgroup Nombre</div></div>
                <div className={styles.Image}><img src={Linux10} alt="" /></div>
                <h3>Una vez creados los 4 grupos, podemos agregar usuarios con el siguiente comando.</h3>
                <div className={styles.Codigo}><div className={styles.LineaCodigo}>sudo adduser NombreUsuario NombreGrupo</div></div>
                <div className={styles.Image}><img src={Linux11} alt="" /></div>
                <h3>Cuando terminemos de asignar usuarios a grupos, podemos ejecutar el siguiente comando para comprobar.</h3>
                <div className={styles.Codigo}><div className={styles.LineaCodigo}>cat /etc/group</div></div>
                <div className={styles.Image}><img src={Linux12} alt="" /></div>
            </div>
        );
    }
}

export default Tarea11;