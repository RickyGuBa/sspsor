import React, { Component } from 'react';
// Styles
import styles from "./Tarea08.module.css";
// Components
import TitleComponent from '../../Components/TitleComponent/TitleComponent';
// Images
import Windows1 from "./img/Windows1.png";
import Windows2 from "./img/Windows2.png";
import Windows3 from "./img/Windows3.png";
import Windows4 from "./img/Windows4.png";
import Windows5 from "./img/Windows5.png";
import Windows6 from "./img/Windows6.png";
import Windows7 from "./img/Windows7.png";
import Windows8 from "./img/Windows8.png";
import Windows9 from "./img/Windows9.png";
import Windows10 from "./img/Windows10.png";
import Windows11 from "./img/Windows11.png";
import Windows12 from "./img/Windows12.png";

class Tarea08 extends Component {
    render() {
        return (
            <div className={styles.Tarea}>
                <TitleComponent>Instalar windows server y destacar las características de Active Directory y ejemplos.</TitleComponent>
                <h2>Características</h2>
                <li className={styles.Requisito}>Administración simplificada de usuarios y recursos de red.</li>
                <li className={styles.Requisito}>Sistemas de autenticación y autorización flexibles y seguros.</li>
                <li className={styles.Requisito}>Consolidación de directorios.</li>
                <li className={styles.Requisito}>Infraestructura y aplicaciones habilitadas para el uso de directorios.</li>
                <li className={styles.Requisito}>Escalabilidad sin complejidad.</li>
                <li className={styles.Requisito}>Uso de los estándares de Internet.</li>
                <li className={styles.Requisito}>Desarrollo eficaz.</li>
                <li className={styles.Requisito}>Replicación y supervisión de confianza.</li>
                <li className={styles.Requisito}>Listas de distribución de Servicios de Message Queue Server.</li>
                <h2>Requisitos de instalación</h2>
                <li className={styles.Requisito}>Computadora</li>
                <li className={styles.Requisito}>USB Booteable</li>
                <li className={styles.Requisito}>ISO Windows Server</li>
                <h2>Instalación</h2>
                <h3>Cargamos la USB con el ISO y procedemos a ejecurarla como en la instalación de Windows y Linux.</h3>
                <h3>Seleccionamos el idioma que queremos.</h3>
                <div className={styles.Image}><img src={Windows1} alt="" /></div>
                <h3>Se iniciará el asistente para instalación, y damos click en "Instalar ahora".</h3>
                <div className={styles.Image}><img src={Windows2} alt="" /></div>
                <h3>
                    Aparecerá una ventana en donde se solicita la clave del producto. Si no tenemos la clave, desmarcamos la casilla 
                    y damos click en siguiente.
                </h3>
                <div className={styles.Image}><img src={Windows3} alt="" /></div>
                <h3>Ahora seleccionamos la instalación que corresponde (Standard).</h3>
                <div className={styles.Image}><img src={Windows4} alt="" /></div>
                <h3>Aceptamos los términos de licencia.</h3>
                <div className={styles.Image}><img src={Windows5} alt="" /></div>
                <h3>Seleccionamos instalación personalizada.</h3>
                <div className={styles.Image}><img src={Windows6} alt="" /></div>
                <h3>Seleccionamos el espacio de disco en el que se va a instalar.</h3>
                <div className={styles.Image}><img src={Windows7} alt="" /></div>
                <h3>Ahora el asistente comenzará con la instalación.</h3>
                <div className={styles.Image}><img src={Windows8} alt="" /></div>
                <h3>Después de instalar, el equipo se reiniciará.</h3>
                <div className={styles.Image}><img src={Windows9} alt="" /></div>
                <h3>Una vez reiniciado, el sistema operativo nos dirá que es necesario hacer un cambio de contraseña por ser la primera vez.</h3>
                <div className={styles.Image}><img src={Windows10} alt="" /></div>
                <h3>Realizamos el cambio de contraseña.</h3>
                <div className={styles.Image}><img src={Windows11} alt="" /></div>
                <h3>Después del cambido de contraseña, se iniciara Windows Server y estará listo para su uso.</h3>
                <div className={styles.Image}><img src={Windows12} alt="" /></div>
            </div>
        );
    }
}

export default Tarea08;