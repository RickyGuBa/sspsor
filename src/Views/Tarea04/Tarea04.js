import React, { Component } from 'react';
// Styles
import styles from "./Tarea04.module.css";
// Components
import TitleComponent from '../../Components/TitleComponent/TitleComponent';
import SOSelector from '../../Components/SOSelector/SOSelector';
// Icons
import { IoLogoWindows, IoLogoTux } from "react-icons/io";
// Images
import Windows1 from "./img/Windows1.png";
import Windows2 from "./img/Windows2.png";
import Windows3 from "./img/Windows3.png";
import Windows4 from "./img/Windows4.png";
import Windows5 from "./img/Windows5.png";
import Windows6 from "./img/Windows6.png";
import Linux1 from "./img/Linux1.png";
import Linux2 from "./img/Linux2.png";
import Linux3 from "./img/Linux3.png";
import Linux4 from "./img/Linux4.png";
import Linux5 from "./img/Linux5.png";
import Linux6 from "./img/Linux6.png";
import Linux7 from "./img/Linux7.png";

const InstallWindows = () => {
    return (
        <div>
            <h3>
                Entramos al sitio oficial de <a target="_blank" rel="noopener noreferrer" href="https://www.oracle.com/technetwork/es/java/javase/downloads/index.html">Oracle</a> y seleccionamos la opción que contiene JDK y Netbeans.
            </h3>
            <div className={styles.Image}><img src={Windows1} alt="" /></div>
            <h3>Nos redireccionará a otra página donde seleccionamos la descarga que corresponda al sistema operativo Windows.</h3>
            <div className={styles.Image}><img src={Windows2} alt="" /></div>
            <h3>
                Una vez descargado el archivo, vamos a ejecutarlo y comenzará un asistente para la instalación.
            </h3>
            <div className={styles.Image}><img src={Windows3} alt="" /></div>
            <h3>Después de pasar por pasos de configuración donde indicamos la ruta para la instalación, la instalación comenzará.</h3>
            <div className={styles.Image}><img src={Windows4} alt="" /></div>
            <h3>Al final el asistente nos indicará que terminó la instalación y damos click en "Finalizar".</h3>
            <div className={styles.Image}><img src={Windows5} alt="" /></div>
        </div>
    );
};

const InstallLinux = () => {
    return (
        <div>
            <h3>
                Entramos al sitio oficial de <a target="_blank" rel="noopener noreferrer" href="https://www.oracle.com/technetwork/es/java/javase/downloads/index.html">Oracle</a> y seleccionamos la opción que contiene JDK y Netbeans.
            </h3>
            <div className={styles.Image}><img src={Linux1} alt="" /></div>
            <h3>Nos redireccionará a otra página donde seleccionamos la descarga que corresponda al sistema operativo Linux.</h3>
            <div className={styles.Image}><img src={Linux2} alt="" /></div>
            <h3>Abrimos una terminal en el directorio del archivo descargado, e ingresamos el siguiente comando.</h3>
            <div className={styles.Codigo}><div className={styles.LineaCodigo}>sudo ./jdk-8u111-nb-8_2-linux-i586.sh</div></div>
            <div className={styles.Image}><img src={Linux3} alt="" /></div>
            <h3>Después de ingresar el comando de consola se ejecutará el archivo y comenzará un asistente para la instalación.</h3>
            <div className={styles.Image}><img src={Linux4} alt="" /></div>
            <h3>Después de pasar por pasos de configuración donde indicamos la ruta para la instalación, la instalación comenzará.</h3>
            <div className={styles.Image}><img src={Linux5} alt="" /></div>
            <h3>Al final el asistente nos indicará que terminó la instalación y damos click en "Finalizar".</h3>
            <div className={styles.Image}><img src={Linux6} alt="" /></div>
        </div>
    );
};

const SoftwareWindows = () => {
    return (
        <div>
            <div className={styles.Image}><img src={Windows6} alt="" /></div>
        </div>
    );
};

const SoftwareLinux = () => {
    return (
        <div>
            <div className={styles.Image}><img src={Linux7} alt="" /></div>
        </div>
    );
};

class Tarea04 extends Component {

    constructor(props) {
        super(props);
        this.state = {
            install_OS: "Windows",
            software_OS: "Windows",
            systems: [
                {name: "Windows", icon: IoLogoWindows},
                {name: "Linux", icon: IoLogoTux},
            ]
        }
    }

    handleChangeInstallOS = (OS) => {
        this.setState({
            install_OS: OS
        })
    }
    
    handleChangeSoftwareOS = (OS) => {
        this.setState({
            software_OS: OS
        })
    }
    
    renderInstallOS = () => {
        switch (this.state.install_OS) {
            case "Windows":
                return <InstallWindows />
            case "Linux":
                return <InstallLinux />
            default:
                break;
        }
    }
    
    renderSoftwareOS = () => {
        switch (this.state.software_OS) {
            case "Windows":
                return <SoftwareWindows />
            case "Linux":
                return <SoftwareLinux />
            default:
                break;
        }
    }

    render() {
        return (
            <div className={styles.Tarea}>
                <TitleComponent>Instalar Java y desarrollar un punto de venta básico que funcione en Windows y Linux.</TitleComponent>
                <h1>Instalación Java.</h1>
                <SOSelector systems={this.state.systems} selected={this.state.install_OS} onChange={this.handleChangeInstallOS}/>
                {this.renderInstallOS()}
                <h1>Punto de venta.</h1>
                <h3><a target="_blank" rel="noopener noreferrer" href="https://www.dropbox.com/s/wzh8l5j477rjz8n/ProyectoCompleto.zip">Código fuente</a></h3>
                <h3>
                    El software consta de un punto de venta en el cual tenemos distintas funcionalidaes como agregar productos, buscar productos,
                    generar reportes y algunas otras funciones.
                </h3>
                <h1>Ejecución punto de venta.</h1>
                <SOSelector systems={this.state.systems} selected={this.state.software_OS} onChange={this.handleChangeSoftwareOS}/>
                {this.renderSoftwareOS()}
            </div>
        );
    }
}

export default Tarea04;