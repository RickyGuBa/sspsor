import React, { Component } from 'react';
// Styles
import styles from "./Tarea06.module.css";
// Components
import TitleComponent from '../../Components/TitleComponent/TitleComponent';
import SOSelector from '../../Components/SOSelector/SOSelector';
// Icons
import { IoLogoWindows, IoLogoTux } from "react-icons/io";
// Images
import Windows1 from "./img/Windows1.png";
import Windows2 from "./img/Windows2.png";
import Windows3 from "./img/Windows3.png";
import Windows4 from "./img/Windows4.png";
import Windows5 from "./img/Windows5.png";
import Windows6 from "./img/Windows6.png";
import Windows7 from "./img/Windows7.png";
import Windows8 from "./img/Windows8.png";
import Windows9 from "./img/Windows9.png";
import Windows10 from "./img/Windows10.png";
import Windows11 from "./img/Windows11.png";
import Windows12 from "./img/Windows12.png";
import Linux1 from "./img/Linux1.png";
import Linux2 from "./img/Linux2.png";
import Linux3 from "./img/Linux3.png";
import Linux4 from "./img/Linux4.png";
import Linux5 from "./img/Linux5.png";
import Linux6 from "./img/Linux6.png";
import Linux7 from "./img/Linux7.png";
import Linux8 from "./img/Linux8.png";
import Linux9 from "./img/Linux9.png";
import Linux10 from "./img/Linux10.png";
import Linux11 from "./img/Linux11.png";

const WindowsRuby = () => {
    return (
        <div>
            <h2>Instalación Windows</h2>
            <h3>Como requisito necesitamos tener instalado Yarn.</h3>
            <h3>Vamos al sitio official de <a target="_blank" rel="noopener noreferrer" href="https://rubyinstaller.org/downloads/">Ruby</a> y descargamos la versión que corresponde a nuestro SO.</h3>
            <div className={styles.Image}><img src={Windows1} alt="" /></div>
            <h3>Una vez descargado el instalador lo ejecutamos y se nos guiará en algunos pasos de configuración.</h3>
            <div className={styles.Image}><img src={Windows2} alt="" /></div>
            <h3>Una vez que terminemos con el asistente de instalación, se abrirá una terminal en donde seguimos con la configuración, para usar la recomendada sólo necesitamos presionar la tecla ENTER.</h3>
            <div className={styles.Image}><img src={Windows3} alt="" /></div>
            <h3>Una vez instalado Ruby, vamos a instalar Rails, para lo cual abrimos una terminarl e ingresamos el siguiente comando.</h3>
            <div className={styles.Codigo}><div className={styles.LineaCodigo}>gem install rails</div></div>
            <div className={styles.Image}><img src={Windows4} alt="" /></div>
            <h3>Después abrimos una terminal en el directorio donde queremos crear el nuevo proyecto e ingresamos el siguiente comando.</h3>
            <div className={styles.Codigo}><div className={styles.LineaCodigo}>rails new app</div></div>
            <div className={styles.Image}><img src={Windows5} alt="" /></div>
            <h3>Una vez que se termino de crear, entramos al directorio del proyecto con la terminal y ejecutamos el siguiente comando.</h3>
            <div className={styles.Codigo}><div className={styles.LineaCodigo}>rails server</div></div>
            <div className={styles.Image}><img src={Windows6} alt="" /></div>
            <h3>Una vez que tenemos corriendo el proyecto, podemos acceder mediante el navegador al <a target="_blank" rel="noopener noreferrer" href="localhost:3000">localhost:3000</a>.</h3>
            <div className={styles.Image}><img src={Windows7} alt="" /></div>
        </div>
    );
};

const LinuxRuby = () => {
    return (
        <div>
            <h2>Instalación Linux</h2>
            <h3>Como requisito necesitamos tener instalado Yarn y gpg2.</h3>
            <h3>Primero abrimos una terminal e ingresamos el siguiente comando para generar una key, que es necesaria para la instalación de RVM.</h3>
            <div className={styles.Codigo}><div className={styles.LineaCodigo}>gpg2 --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB</div></div>
            <div className={styles.Image}><img src={Linux1} alt="" /></div>
            <h3>Ahora con el siguiente comando instalamos RVM junto con Ruby y Rails.</h3>
            <div className={styles.Codigo}><div className={styles.LineaCodigo}>\curl -sSL https://get.rvm.io | bash -s stable --rails</div></div>
            <div className={styles.Image}><img src={Linux2} alt="" /></div>
            <h3>Ahora agregamos la fuente de RVM con el siguiente comando.</h3>
            <div className={styles.Codigo}><div className={styles.LineaCodigo}>source .rvm/scripts/rvm</div></div>
            <div className={styles.Image}><img src={Linux3} alt="" /></div>
            <h3>Después abrimos una terminal en el directorio donde queremos crear el nuevo proyecto e ingresamos el siguiente comando.</h3>
            <div className={styles.Codigo}><div className={styles.LineaCodigo}>rails new app</div></div>
            <div className={styles.Image}><img src={Linux4} alt="" /></div>
            <h3>Una vez que se termino de crear, entramos al directorio del proyecto con la terminal y ejecutamos el siguiente comando.</h3>
            <div className={styles.Codigo}><div className={styles.LineaCodigo}>rails s</div></div>
            <div className={styles.Image}><img src={Linux5} alt="" /></div>
            <h3>Una vez que tenemos corriendo el proyecto, podemos acceder mediante el navegador al <a target="_blank" rel="noopener noreferrer" href="localhost:3000">localhost:3000</a>.</h3>
            <div className={styles.Image}><img src={Linux6} alt="" /></div>
        </div>
    );
};

const WindowsIonic = () => {
    return (
        <div>
            <h2>Instalación Windows</h2>
            <h3>Vamos al sitio official de <a target="_blank" rel="noopener noreferrer" href="https://nodejs.org/en/">NodeJS</a> y descargamos la versión recomendada.</h3>
            <div className={styles.Image}><img src={Windows8} alt="" /></div>
            <h3>Una vez descargado el instalador lo ejecutamos y se nos guiará en algunos pasos de configuración, y al final se hará el proceso de instalación.</h3>
            <div className={styles.Image}><img src={Windows9} alt="" /></div>
            <h3>Ahora pasamos a instalar Ionic con el siguiente comando.</h3>
            <div className={styles.Codigo}><div className={styles.LineaCodigo}>npm install -g ionic</div></div>
            <div className={styles.Image}><img src={Windows10} alt="" /></div>
            <h3>Para la aplicación básica usaremos el codigo fuente de <a target="_blank" rel="noopener noreferrer" href="https://gitlab.com/jaregzz11/ifisica">GitLab</a></h3>
            <h3>Una vez descargado abrimos una terminal dentro del directorio del proyecto e ingresar los siguientes comandos.</h3>
            <div className={styles.Codigo}>
                <div className={styles.LineaCodigo}>npm install</div>
                <div className={styles.LineaCodigo}>ionic serve</div>
            </div>
            <div className={styles.Image}><img src={Windows11} alt="" /></div>
            <h3>Una vez que tenemos corriendo el proyecto, podemos acceder mediante el navegador al <a target="_blank" rel="noopener noreferrer" href="localhost:8100">localhost:8100</a>.</h3>
            <div className={styles.Image}><img src={Windows12} alt="" /></div>
        </div>
    );
};

const LinuxIonic = () => {
    return (
        <div>
            <h2>Instalación Windows</h2>
            <h3>Instalamos NodeJS ingresando el siguiente comando.</h3>
            <div className={styles.Codigo}><div className={styles.LineaCodigo}>sudo apt-get install nodejs</div></div>
            <div className={styles.Image}><img src={Linux7} alt="" /></div>
            <h3>Instalamos npm ingresando el siguiente comando.</h3>
            <div className={styles.Codigo}><div className={styles.LineaCodigo}>sudo apt-get install npm</div></div>
            <div className={styles.Image}><img src={Linux8} alt="" /></div>
            <h3>Ahora instalamos ionic con el siguiente comando.</h3>
            <div className={styles.Codigo}><div className={styles.LineaCodigo}>npm install -g ionic</div></div>
            <div className={styles.Image}><img src={Linux9} alt="" /></div>
            <h3>Para la aplicación básica usaremos el codigo fuente de <a target="_blank" rel="noopener noreferrer" href="https://gitlab.com/jaregzz11/ifisica">GitLab</a></h3>
            <h3>Una vez descargado abrimos una terminal dentro del directorio del proyecto e ingresar los siguientes comandos.</h3>
            <div className={styles.Codigo}>
                <div className={styles.LineaCodigo}>npm install</div>
                <div className={styles.LineaCodigo}>ionic serve</div>
            </div>
            <div className={styles.Image}><img src={Linux10} alt="" /></div>
            <h3>Una vez que tenemos corriendo el proyecto, podemos acceder mediante el navegador al <a target="_blank" rel="noopener noreferrer" href="localhost:8100">localhost:8100</a>.</h3>
            <div className={styles.Image}><img src={Linux11} alt="" /></div>
        </div>
    );
};

class Tarea06 extends Component {

    constructor(props) {
        super(props);
        this.state = {
            rubyOS: "Windows",
            ionicOS: "Windows",
            systems: [
                {name: "Windows", icon: IoLogoWindows},
                {name: "Linux", icon: IoLogoTux},
            ]
        }
    }

    handleChangeRuby = (OS) => {
        this.setState({
            rubyOS: OS
        })
    }
    
    handleChangeIonic = (OS) => {
        this.setState({
            ionicOS: OS
        })
    }

    render() {
        return (
            <div className={styles.Tarea}>
                <TitleComponent>Instalar Ruby On Rails e Ionic, y desarrollar una aplicación básica.</TitleComponent>
                <h1>Ruby On Rails</h1>
                <SOSelector systems={this.state.systems} selected={this.state.rubyOS} onChange={this.handleChangeRuby}/>
                { this.state.rubyOS === "Windows" ? <WindowsRuby /> : <LinuxRuby /> }
                <h1>Ionic</h1>
                <SOSelector systems={this.state.systems} selected={this.state.ionicOS} onChange={this.handleChangeIonic}/>
                { this.state.ionicOS === "Windows" ? <WindowsIonic /> : <LinuxIonic /> }
            </div>
        );
    }
}

export default Tarea06;