import React, { Component } from 'react';
// Styles
import styles from "./Tarea13.module.css";
// Components
import TitleComponent from '../../Components/TitleComponent/TitleComponent';
// Images
import Samba1 from "./img/Samba1.png";
import Samba2 from "./img/Samba2.png";
import Samba3 from "./img/Samba3.png";
import Samba4 from "./img/Samba4.png";
import Samba5 from "./img/Samba5.png";
import Samba6 from "./img/Samba6.png";
import Samba7 from "./img/Samba7.png";
import Samba8 from "./img/Samba8.png";
import Samba9 from "./img/Samba9.png";

class Tarea13 extends Component {
    render() {
        return (
            <div className={styles.Tarea}>
                <TitleComponent>
                    Instalar Samba en Windows y Linux, y configurar para compartir carpetas, archivos e impresoras. 
                </TitleComponent>
                <h2>Linux</h2>
                <h3>Instalamos Samba utilizando el siguiente comando en la terminal.</h3>
                <div className={styles.Codigo}><div className={styles.LineaCodigo}>sudo apt-get install samba</div></div>
                <div className={styles.Image}><img src={Samba1} alt="" /></div>
                <h3>Generamos un nuevo usuario para utilizarlo con samba, con el siguiente comando.</h3>
                <div className={styles.Codigo}><div className={styles.LineaCodigo}>sudo smbpasswd -a NombreUsuario</div></div>
                <div className={styles.Image}><img src={Samba2} alt="" /></div>
                <h3>Vamos a modoficar el archivo de configuración de Samba con el siguiente comando.</h3>
                <div className={styles.Codigo}><div className={styles.LineaCodigo}>sudo nano /etc/samba/smb.conf</div></div>
                <div className={styles.Image}><img src={Samba3} alt="" /></div>
                <h3>Agregamos nuestra carpeta a compartir con el siguiente formato.</h3>
                <div className={styles.Image}><img src={Samba4} alt="" /></div>
                <h3>Guardamos los cambios y reiniciamos Samba con el siguiente comando.</h3>
                <div className={styles.Codigo}><div className={styles.LineaCodigo}>sudo systemctl restart smbd nmbd</div></div>
                <div className={styles.Image}><img src={Samba5} alt="" /></div>
                <h2>Windows</h2>
                <h3>Presionamos la tecla Windows + R, y escribimos la IP de Linux.</h3>
                <div className={styles.Image}><img src={Samba6} alt="" /></div>
                <h3>Se nos pedirá usuario y contraseña, utilizamos el mismo usuario que creamos en Linux.</h3>
                <div className={styles.Image}><img src={Samba7} alt="" /></div>
                <h3>Una vez que ingresamos, podemos ver la carpeta compartida a la que llamamos Samba.</h3>
                <div className={styles.Image}><img src={Samba8} alt="" /></div>
                <h3>Si entramos a la carpeta podemos ver los archivos que se han compartido desde Linux en esa carpeta.</h3>
                <div className={styles.Image}><img src={Samba9} alt="" /></div>
            </div>
        );
    }
}

export default Tarea13;