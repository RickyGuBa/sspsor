import React, { Component } from 'react';
// Styles
import styles from "./Tarea02.module.css"
// Components
import TitleComponent from '../../Components/TitleComponent/TitleComponent';
import SOSelector from '../../Components/SOSelector/SOSelector';
// Icons
import { IoLogoWindows, IoLogoTux, IoLogoApple } from "react-icons/io";
// Images
import Windows1 from "./img/Windows.png";
import Linux1 from "./img/Linux.png";
import MacOS1 from "./img/MacOS.png";

const Windows = () => {
    return (
        <div>
            <h2>Ejecución en Windows</h2>
            <div className={styles.Image}><img src={Windows1} alt="" /></div>
        </div>
    );
};

const Linux = () => {
    return (
        <div>
            <h2>Ejecución en Linux</h2>
            <div className={styles.Image}><img src={Linux1} alt="" /></div>
        </div>
    );
};

const MacOS = () => {
    return (
        <div>
            <h2>Ejecución en MacOS</h2>
            <div className={styles.Image}><img src={MacOS1} alt="" /></div>
        </div>
    );
};

class Tarea02 extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
            OS: "Windows",
            systems: [
                {name: "Windows", icon: IoLogoWindows},
                {name: "Linux", icon: IoLogoTux},
                {name: "MacOS", icon: IoLogoApple},
            ]
        }
    }
    
    handleChangeOS = (OS) => {
        this.setState({
            OS
        })
    }

    renderOS = () => {
        switch (this.state.OS) {
            case "Windows":
                return <Windows />
            case "Linux":
                return <Linux />
            case "MacOS":
                return <MacOS />
            default:
                break;
        }
    }

    render() {
        return (
            <div className={styles.Tarea}>
                <TitleComponent>Desarrollar un programa para ver los servicios de Windows y Linux.</TitleComponent>
                <h1>Programa</h1>
                <h3>El programa está desarrollado en Python 3.</h3>
                <div className={styles.Codigo}>
                    <div className={styles.LineaCodigo}>import os</div>
                    <div className={styles.LineaCodigo}>import platform</div>
                    <div className={styles.LineaCodigo}>SO = platform.system()</div>
                    <div className={styles.LineaCodigo}>command = ""</div>
                    <div className={styles.LineaCodigo}>if(SO == "Darwin" or SO == "Linux"):</div>
                    <div className={styles.LineaCodigoTab}>command = "top"</div>
                    <div className={styles.LineaCodigo}>else:</div>
                    <div className={styles.LineaCodigoTab}>command = "tasklist"</div>
                    <div className={styles.LineaCodigo}>os.system(command)</div>
                </div>
                <SOSelector systems={this.state.systems} selected={this.state.OS} onChange={this.handleChangeOS}/>
                {this.renderOS()}
            </div>
        );
    }
}

export default Tarea02;