import React, { Component } from 'react';
// Styles
import styles from "./Tarea12.module.css";
// Components
import TitleComponent from '../../Components/TitleComponent/TitleComponent';
// Images
import Linux1 from "./img/Linux1.png";
import Linux2 from "./img/Linux2.png";
import Linux3 from "./img/Linux3.png";
import Django from "./../Tarea10/img/Django1.jpg";
import Rails from "./../Tarea10/img/Rails1.jpg";

class Tarea12 extends Component {
    render() {
        return (
            <div className={styles.Tarea}>
                <TitleComponent>
                    Instalar un sistema en Linux Server con Django con bases de datos y uno en Ruby On Rails con bases de datos y asignar 
                    diferentes permisos de acceso a los grupos de usuarios en cada sistema. 
                </TitleComponent>
                <h2>Proyectos</h2>
                <h3>Tenemos los dos proyectos tanto de Django (mysite) y Ruby (app).</h3>
                <div className={styles.Image}><img src={Linux1} alt="" /></div>
                <h3>Estructura proyecto Django:</h3>
                <div className={styles.Image}><img src={Django} alt="" style={{height: "5rem"}}/></div>
                <h3>Estructura proyecto Ruby:</h3>
                <div className={styles.Image}><img src={Rails} alt="" /></div>
                <h2>Django</h2>
                <h3>Para cambiar los permisos es necesario estar con el usuario propietario.</h3>
                <h3>
                    En este caso queremos dar permisos de Leer, Ejecutar y Escribir al propietario, permisos de Leer y Ejecutar para el 
                    grupo, y permiso de Leer para otros, por lo tengo ingresamos el siguiente comando, lo cual corresponde al directorio del 
                    proyecto de Django.
                </h3>
                <div className={styles.Codigo}><div className={styles.LineaCodigo}>chmod 764 mysite/</div></div>
                <div className={styles.Image}><img src={Linux2} alt="" /></div>
                <h2>Ruby On Rails</h2>
                <h3>Para cambiar los permisos es necesario estar con el usuario propietario.</h3>
                <h3>
                    En este caso queremos dar permisos de Leer, Ejecutar y Escribir al propietario, permisos de Leer y Ejecutar para el 
                    grupo, y permiso de Leer para otros, por lo tengo ingresamos el siguiente comando, lo cual corresponde al directorio del 
                    proyecto de Ruby.
                </h3>
                <div className={styles.Codigo}><div className={styles.LineaCodigo}>chmod 764 app/</div></div>
                <div className={styles.Image}><img src={Linux3} alt="" /></div>
            </div>
        );
    }
}

export default Tarea12;