import React, { Component } from 'react';
// Styles
import styles from "./Home.module.css"

class Home extends Component {

    constructor(props) {
        super(props);
        this.state = {
            tareas : []
        }
    }
    
    componentDidMount() {
        let tareas = [
            "Instalar Windows y Linux",
            "Ver servicios Windows y Linux",
            "Instalar Libre Office en Windows y Linux",
            "Instalar Java y hacer Punto de venta Windows y Linux",
            "Instalar Django y Laraval, y hacer app",
            "Instalar Ruby on rails e Ionic y hacer app",
            "Videojuego en Windows y Linux",
            "Instalar Windows Server",
            "Instalar 10 Usuarios y 4 grupos",
            "Windows Server + Django y Ruby on rails con permisos",
            "Instalar Linux Server y configurar usuarios",
            "Instalar Linux Server + Django y Ruby on rails con permisos",
            "Instalar Samba en Windows y Linux",
        ]
        this.setState({
            tareas
        })
    }
    

    handleOnClick = (num) => {
        this.props.history.push(`/tarea/${num}`);
    }

    render() {
        return (
            <div className={styles.Home}>
                <div className={styles.HomeHeader}>Seminario de solución de problemas de sistemas operativos de red</div>
                <div className={styles.HomeList}>
                    {
                        this.state.tareas.map((tarea, index) => 
                            <div className={styles.HomeListItem} onClick={() => this.handleOnClick(index + 1)} key={index}><span>{index + 1}. {tarea}</span></div>
                        )
                    }
                </div>
            </div>
        );
    }
}

export default Home;