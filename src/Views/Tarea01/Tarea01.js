import React, { Component } from 'react';
// Components
import TitleComponent from '../../Components/TitleComponent/TitleComponent';
// Icons
import { IoLogoWindows, IoLogoTux } from "react-icons/io";
// Styles
import styles from "./Tarea01.module.css";
// Images
import Tabla from "./img/Tabla.png";
import General1 from "./img/General1.jpg";
import General2 from "./img/General2.JPG";

import Windows1 from "./img/Windows1.JPG";
import Windows2 from "./img/Windows2.JPG";
import Windows3 from "./img/Windows3.JPG";
import Windows4 from "./img/Windows4.JPG";
import Windows5 from "./img/Windows5.JPG";
import Windows6 from "./img/Windows6.JPG";
import Windows7 from "./img/Windows7.JPG";
import Windows8 from "./img/Windows8.JPG";

import Linux1 from "./img/Linux1.JPG";
import Linux2 from "./img/Linux2.JPG";
import Linux3 from "./img/Linux3.JPG";
import Linux4 from "./img/Linux4.JPG";
import Linux5 from "./img/Linux5.JPG";
import Linux6 from "./img/Linux6.JPG";
import Linux7 from "./img/Linux7.JPG";
import SOSelector from '../../Components/SOSelector/SOSelector';


const Windows = () => {
    return (
        <React.Fragment>
            <h3>Cuando seleccionemos la USB booteable se iniciará el proceso.</h3>
            <div className={styles.Image}><img src={Windows1} alt="" /></div>
            <h3>
                Una vez que se realice la preparación necesaria por parte de windows, aparecerá un menú para seleccionar el 
                idioma que queremos y seleccionamos "Siguiente", después damos click en donde dice "Instalar ahora".
            </h3>
            <div className={styles.Image}><img src={Windows2} alt="" /></div>
            <h3>
                Esperamos a que se inicie el programa de instalación, aceptamos los Términos y Condiciones, en tipo de instalación 
                seleccionamos "Personalizada (avanzada)".
            </h3>
            <div className={styles.Image}><img src={Windows3} alt="" /></div>
            <h3>
                Después debemos seleccionar el disco o partición donde queremos instalar el SO. En mi caso será en la partición 2, 
                seleccionamos la partición y procedemos a Formatearla en las "Opciones de unidad", una vez formateada damos click en 
                "Siguiente".
            </h3>
            <div className={styles.Image}><img src={Windows4} alt="" /></div>
            <h3>
                En este punto, se comienza a instalar Windows 7 con las configuraciónes que seleccionamos.
            </h3>
            <div className={styles.Image}><img src={Windows5} alt="" /></div>
            <h3>
                Al finalizar la instalación, se procede con la configuración inicial de Windows 7, donde indicamos nombre del equipo, 
                nombre del usuario, contraseña, clave del producto para activar Windows, configuración de seguridad y la fecha y hora.
            </h3>
            <div className={styles.Image}><img src={Windows6} alt="" /></div>
            <h3>
                Cuando terminemos de seleccionar la configuración de Windows, el sistema procede a aplicarlas.
            </h3>
            <div className={styles.Image}><img src={Windows7} alt="" /></div>
            <h3>
                Al finalizar con la configuración se inicia el equipo con Windows 7 listo para darle uso.
            </h3>
            <div className={styles.Image}><img src={Windows8} alt="" /></div>
        </React.Fragment>
    );
};

const Linux = () => {
    return (
        <React.Fragment>
            <h3>Al inicio nos aparece el Installer boot menu de Peppermint, seleccionamos la opción "Install Peppermint OS"</h3>
            <div className={styles.Image}><img src={Linux1} alt="" /></div>
            <h3>
                Después pasara a la parte de configuración de la instalación, en donde seleccionamos idioma, disposición de teclado, 
                red inalámbrica, si queremos instalar complementos como juegos o simplemente lo escencial.
            </h3>
            <div className={styles.Image}><img src={Linux2} alt="" /></div>
            <h3>
                En "Tipo de instalación" seleccionamos como queremos instalar el SO, en este caso seleccionamos "Borrar disco e 
                instalar Peppermint".
            </h3>
            <div className={styles.Image}><img src={Linux3} alt="" /></div>
            <h3>
                Después aparecerá un mapa donde seleccionamos nuestra zona horaria, y luego un formulario donde ingresamos nombre de 
                usuario, nombre del equipo y contraseña.
            </h3>
            <div className={styles.Image}><img src={Linux4} alt="" /></div>
            <h3>
                Después de terminar con la configuración, el programa va a comenzar con la instalación del sistema operativo.
            </h3>
            <div className={styles.Image}><img src={Linux5} alt="" /></div>
            <h3>
                Una vez finalizada la instalación, nos saldrá un mensaje diciendo que el equipo se reiniciará para usar el sistema.
            </h3>
            <div className={styles.Image}><img src={Linux6} alt="" /></div>
            <h3>
                Una vez que terminó de reiniciarse el equipo, se iniciará el sistema operativo Peppermint y estará listo para su uso.
            </h3>
            <div className={styles.Image}><img src={Linux7} alt="" /></div>
        </React.Fragment>
    );
};

class Tarea01 extends Component {

    constructor(props) {
        super(props);
        this.state = {
            OS: "Windows",
            systems: [
                {name: "Windows", icon: IoLogoWindows},
                {name: "Linux", icon: IoLogoTux},
            ]
        }
    }
    
    handleChangeOS = (OS) => {
        this.setState({
            OS
        })
    }

    render() {
        return (
            <div className={styles.Tarea}>
                <TitleComponent>Instalar windows y linux desktop nativo, y características sobresalientes de cada sistema operativo</TitleComponent>
                <div className={styles.Caracteristicas}>
                    <h1>Características</h1>
                    <div className={styles.CaracteristicasImg}>
                        <img src={Tabla} alt=""/>
                        <div className={styles.CaracteristicasRef}>Fuente: <a target="_blank" rel="noopener noreferrer" href="https://www.ionos.mx/digitalguide/servidores/know-how/linux-vs-windows-el-gran-cuadro-comparativo/">https://www.ionos.mx/digitalguide/servidores/know-how/linux-vs-windows-el-gran-cuadro-comparativo/</a></div>
                    </div>
                </div>
                <div className={styles.Instalacion}>
                    <h1>Instalación</h1>
                    <h2>Requisitos:</h2>
                    <li className={styles.Requisito}>Computadora (MSI U130)</li>
                    <li className={styles.Requisito}>USB Booteable (SanDisk 16GB)</li>
                    <li className={styles.Requisito}>ISO Windows (Windows 7)</li>
                    <li className={styles.Requisito}>ISO Linux (Peppermint 10)</li>
                    <h2>Procedimiento:</h2>
                    <h3>
                        Cargar la USB booteable con el archivo ISO correspondiente al sistema operativo a instalar. 
                        En este caso se utilizó el programa <a target="_blank" rel="noopener noreferrer" href="https://rufus.ie/es_ES.html">Rufus</a>.
                    </h3>
                    <h3>Conectar la USB y encender la computadora, se debe acceder al Boot Menu (F11).</h3>
                    <div className={styles.Image}><img src={General1} alt="" /></div>
                    <h3>Una vez dentro del menú, seleccionar la USB para iniciar el arranque.</h3>
                    <div className={styles.Image}><img src={General2} alt="" /></div>
                    <SOSelector systems={this.state.systems} selected={this.state.OS} onChange={this.handleChangeOS}/>
                    { this.state.OS === "Windows" ? <Windows /> : <Linux /> }
                </div>
            </div>
        );
    }
}

export default Tarea01;