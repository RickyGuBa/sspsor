import React, { Component } from 'react';
// Styles
import styles from "./Tarea05.module.css";
// Components
import TitleComponent from '../../Components/TitleComponent/TitleComponent';
import SOSelector from '../../Components/SOSelector/SOSelector';
// Icons
import { IoLogoWindows, IoLogoTux } from "react-icons/io";
// Images
import Windows1 from "./img/Windows1.png";
import Windows2 from "./img/Windows2.png";
import Windows3 from "./img/Windows3.png";
import Windows4 from "./img/Windows4.png";
import Windows5 from "./img/Windows5.png";
import Windows6 from "./img/Windows6.png";
import Windows7 from "./img/Windows7.png";
import Windows8 from "./img/Windows8.png";
import Windows9 from "./img/Windows9.png";
import Windows10 from "./img/Windows10.png";
import Windows11 from "./img/Windows11.png";
import Windows12 from "./img/Windows12.png";
import Linux1 from "./img/Linux1.png";
import Linux2 from "./img/Linux2.png";
import Linux3 from "./img/Linux3.png";
import Linux4 from "./img/Linux4.png";
import Linux5 from "./img/Linux5.png";
import Linux6 from "./img/Linux6.png";
import Linux7 from "./img/Linux7.png";
import Linux8 from "./img/Linux8.png";
import Linux9 from "./img/Linux9.png";
import Linux10 from "./img/Linux10.png";
import Linux11 from "./img/Linux11.png";
import Linux12 from "./img/Linux12.png";
import Linux13 from "./img/Linux13.png";
import Linux14 from "./img/Linux14.png";
import Linux15 from "./img/Linux15.png";


const WindowsDjango = () => {
    return (
        <div>
            <h2>Instalación Windows</h2>
            <h3>Para la instalación es requerido tener Python instalado, podemos verificarlo con el siguiente comando.</h3>
            <div className={styles.Codigo}><div className={styles.LineaCodigo}>python --version</div></div>
            <div className={styles.Image}><img src={Windows1} alt="" /></div>
            <h3>Para comenzar abrimos una terminal e ingresamos el siguiente comando.</h3>
            <div className={styles.Codigo}><div className={styles.LineaCodigo}>pip install Django</div></div>
            <div className={styles.Image}><img src={Windows2} alt="" /></div>
            <h3>Una vez finalizada la instalación podemos comprobar que se haya instalado correctamente con el siguiente comando.</h3>
            <div className={styles.Codigo}><div className={styles.LineaCodigo}>python -m django --version</div></div>
            <div className={styles.Image}><img src={Windows3} alt="" /></div>
            <h3>Para comenzar un nuevo proyecto vamos al directorio donde queremos crearlo e ingresamos el siguiente comando.</h3>
            <div className={styles.Codigo}><div className={styles.LineaCodigo}>django-admin startproject mysite</div></div>
            <div className={styles.Image}><img src={Windows4} alt="" /></div>
            <h3>Para correr el proyecto nos dirijimos al directorio creado mediante la terminal e ingresamos el siguiente comando.</h3>
            <div className={styles.Codigo}><div className={styles.LineaCodigo}>python manage.py runserver</div></div>
            <div className={styles.Image}><img src={Windows5} alt="" /></div>
            <h3>Una vez que tenemos corriendo el proyecto, podemos acceder mediante el navegador al <a target="_blank" rel="noopener noreferrer" href="localhost:8000">localhost:8000</a>.</h3>
            <div className={styles.Image}><img src={Windows6} alt="" /></div>
        </div>
    );
};

const LinuxDjango = () => {
    return (
        <div>
            <h2>Instalación Linux</h2>
            <h3>Para la instalación es requerido tener Python instalado, podemos verificarlo con el siguiente comando.</h3>
            <div className={styles.Codigo}><div className={styles.LineaCodigo}>python --version</div></div>
            <div className={styles.Image}><img src={Linux1} alt="" /></div>
            <h3>Para comenzar abrimos una terminal e ingresamos el siguiente comando.</h3>
            <div className={styles.Codigo}><div className={styles.LineaCodigo}>sudo apt-get install python-django</div></div>
            <div className={styles.Image}><img src={Linux2} alt="" /></div>
            <h3>Una vez finalizada la instalación podemos comprobar que se haya instalado correctamente con el siguiente comando.</h3>
            <div className={styles.Codigo}><div className={styles.LineaCodigo}>python -m django --version</div></div>
            <div className={styles.Image}><img src={Linux3} alt="" /></div>
            <h3>Para comenzar un nuevo proyecto vamos al directorio donde queremos crearlo e ingresamos el siguiente comando.</h3>
            <div className={styles.Codigo}><div className={styles.LineaCodigo}>django-admin startproject mysite</div></div>
            <div className={styles.Image}><img src={Linux4} alt="" /></div>
            <h3>Para correr el proyecto nos dirijimos al directorio creado mediante la terminal e ingresamos el siguiente comando.</h3>
            <div className={styles.Codigo}><div className={styles.LineaCodigo}>python manage.py runserver</div></div>
            <div className={styles.Image}><img src={Linux5} alt="" /></div>
            <h3>Una vez que tenemos corriendo el proyecto, podemos acceder mediante el navegador al <a target="_blank" rel="noopener noreferrer" href="localhost:8000">localhost:8000</a>.</h3>
            <div className={styles.Image}><img src={Linux6} alt="" /></div>
        </div>
    );
};

const WindowsLaravel = () => {
    return (
        <div>
            <h2>Instalación Windows</h2>
            <h3>
                Entramos al sitio oficial de <a target="_blank" rel="noopener noreferrer" href="https://laragon.org/download/">Laragon</a> y seleccionamos la descarga de la edición Laragon Full.
            </h3>
            <div className={styles.Image}><img src={Windows7} alt="" /></div>
            <h3>Una vez descargado el archivo procedemos a ejecutarlo.</h3>
            <div className={styles.Image}><img src={Windows8} alt="" /></div>
            <h3>Se nos guiará a través de una serie de pasos de configuración para comenzar la instalación.</h3>
            <div className={styles.Image}><img src={Windows9} alt="" /></div>
            <h3>Una vez que esté instalado, se nos pedirá que reiniciemos el equipo.</h3>
            <h3>Depués ejecutamos Laragon, veremos una ventana donde daremos click en el boton START.</h3>
            <h3>Una vez que esté en ejecución vamos a Menu>Quick app>Laravel y le indicamos el nombre del proyecto.</h3>
            <div className={styles.Image}><img src={Windows10} alt="" /></div>
            <h3>
                En este caso el nombre del proyecto es PruebaLaravel, en la terminal se muestra el avance en la creación del proyecto y al 
                finalizar se comenzará a correr automáticamente.
            </h3>
            <div className={styles.Image}><img src={Windows11} alt="" /></div>
            <h3>Para acceder al proyecto en ejecución vamos al navegador y accedemos a <a target="_blank" rel="noopener noreferrer" href="pruebalaravel.test">pruebalaravel.test</a>.</h3>
            <div className={styles.Image}><img src={Windows12} alt="" /></div>
        </div>
    );
};
const LinuxLaravel = () => {
    return (
        <div>
            <h2>Instalación Laravel</h2>
            <h3>Para comenzar con la instalación de Laravel necesitamos algunos prerequisitos, los instalamos con el siguiente comando.</h3>
            <div className={styles.Codigo}><div className={styles.LineaCodigo}>sudo apt-get install curl mcrypt php-json php-cli</div></div>
            <div className={styles.Image}><img src={Linux7} alt="" /></div>
            <h3>Es necesario instalar composer, por lo tanto vamos poner la terminal en modo de super usuario con el siguiente comando.</h3>
            <div className={styles.Codigo}><div className={styles.LineaCodigo}>sudo su</div></div>
            <div className={styles.Image}><img src={Linux8} alt="" /></div>
            <h3>Navegamos a la ruta /usr/local/bin con el siguiente comando.</h3>
            <div className={styles.Codigo}><div className={styles.LineaCodigo}>cd /usr/local/bin</div></div>
            <div className={styles.Image}><img src={Linux9} alt="" /></div>
            <h3>Ahora ingresamos los siguientes 3 comandos para la instalación de composer.</h3>
            <div className={styles.Codigo}>
                <div className={styles.LineaCodigo}>php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"</div>
                <div className={styles.LineaCodigo}>php composer-setup.php</div>
                <div className={styles.LineaCodigo}>php -r "unlink('composer-setup.php');"</div>
            </div>
            <div className={styles.Image}><img src={Linux10} alt="" /></div>
            <h3>Para finalizar la instalación de composer ingresamos el siguiente comando.</h3>
            <div className={styles.Codigo}><div className={styles.LineaCodigo}>mv composer.phar composer</div></div>
            <div className={styles.Image}><img src={Linux11} alt="" /></div>
            <h3>Ahora navegamos al directorio donde queremos crear la aplicación e ingresamos el siguiente comando.</h3>
            <div className={styles.Codigo}><div className={styles.LineaCodigo}>composer create-project --prefer-dist laravel/laravel app</div></div>
            <div className={styles.Image}><img src={Linux12} alt="" /></div>
            <h3>Al finalizar la creación, es necesario generar una key, para esto entramos al directorio de la app e ingresamos el siguiente comando.</h3>
            <div className={styles.Codigo}><div className={styles.LineaCodigo}>php artisan key:generate</div></div>
            <div className={styles.Image}><img src={Linux13} alt="" /></div>
            <h3>Con esto ahora podemos empezar a correr la aplicación con el siguiente comando.</h3>
            <div className={styles.Codigo}><div className={styles.LineaCodigo}>php artisan serve</div></div>
            <div className={styles.Image}><img src={Linux14} alt="" /></div>
            <h3>Para ver nuestra aplicación corriendo vamos en el navegador a <a target="_blank" rel="noopener noreferrer" href="localhost:8000">localhost:8000</a>.</h3>
            <div className={styles.Image}><img src={Linux15} alt="" /></div>
        </div>
    );
};

class Tarea05 extends Component {

    constructor(props) {
        super(props);
        this.state = {
            djangoOS: "Windows",
            laravelOS: "Windows",
            systems: [
                {name: "Windows", icon: IoLogoWindows},
                {name: "Linux", icon: IoLogoTux},
            ]
        }
    }

    handleChangeDjango = (OS) => {
        this.setState({
            djangoOS: OS
        })
    }
    
    handleChangeLaravel = (OS) => {
        this.setState({
            laravelOS: OS
        })
    }
    
    render() {
        return (
            <div className={styles.Tarea}>
                <TitleComponent>Instalar Django con Python y Laravel, y desarrollar una aplicación básica.</TitleComponent>
                <h1>Django</h1>
                <SOSelector systems={this.state.systems} selected={this.state.djangoOS} onChange={this.handleChangeDjango}/>
                { this.state.djangoOS === "Windows" ? <WindowsDjango /> : <LinuxDjango /> }
                <h1>Laravel</h1>
                <SOSelector systems={this.state.systems} selected={this.state.laravelOS} onChange={this.handleChangeLaravel}/>
                { this.state.laravelOS === "Windows" ? <WindowsLaravel /> : <LinuxLaravel /> }
            </div>
        );
    }
}

export default Tarea05;
