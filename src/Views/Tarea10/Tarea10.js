import React, { Component } from 'react';
// Styles
import styles from "./Tarea10.module.css";
// Components
import TitleComponent from '../../Components/TitleComponent/TitleComponent';
// Images
import Django1 from "./img/Django1.jpg";
import Django2 from "./img/Django2.png";
import Django3 from "./img/Django3.png";
import Django4 from "./img/Django4.png";
import Django5 from "./img/Django5.png";
import Django6 from "./img/Django6.png";
import Django7 from "./img/Django7.png";
import Django8 from "./img/Django8.png";
import Rails1 from "./img/Rails1.jpg";
import Rails2 from "./img/Rails2.png";
import Rails3 from "./img/Rails3.png";
import Rails4 from "./img/Rails4.png";
import Rails5 from "./img/Rails5.png";
import Rails6 from "./img/Rails6.png";

class Tarea10 extends Component {
    render() {
        return (
            <div className={styles.Tarea}>
                <TitleComponent>
                    Instalar un sistema en Windows Server con Django con bases de datos y uno en Ruby On Rails con bases de datos y asignar 
                    diferentes permisos de acceso a los grupos de usuarios en cada sistema.    
                </TitleComponent>
                <h2>Django</h2>
                <h3>Nuestro proyecto tiene de nombre "mysite", y la base de datos que utilizamos está hecha con sqlite3.</h3>
                <div className={styles.Image}><img src={Django1} alt="" style={{height: "5rem"}}/></div>
                <h3>Para dar permisos a los grupos de usuario, damos click derecho al directorio y vamos a la opción "Propiedades".</h3>
                <div className={styles.Image}><img src={Django2} alt="" /></div>
                <h3>Vamos a la pestaña "Compartir", y damos click en "Uso compartido avanzado".</h3>
                <div className={styles.Image}><img src={Django3} alt="" /></div>
                <h3>Marcamos la casilla "Compartir esta carpeta" y ahora vamos a "Permisos".</h3>
                <div className={styles.Image}><img src={Django4} alt="" /></div>
                <h3>Aquí se muestran usuarios y grupos de usuarios con permisos, damos click en "Agregar".</h3>
                <div className={styles.Image}><img src={Django5} alt="" /></div>
                <h3>Escribimos los nombres de los grupos que queremos agregar para compartir, en este caso es el Grupo 1 y Grupo 2.</h3>
                <div className={styles.Image}><img src={Django6} alt="" /></div>
                <h3>
                    Una vez agregados, podemos seleccionarlos y elegir que permisos darle, en este caso dimos "Control todal" al Grupo 1 y 
                    "Cambiar" al Grupo 2.
                </h3>
                <div className={styles.Image}><img src={Django7} alt="" /></div>
                <h3>Una vez aceptados los cambios, nos aparecerá el icono de compartido en la carpeta.</h3>
                <div className={styles.Image}><img src={Django8} alt="" /></div>
                <h2>Ruby On Rails</h2>
                <h3>Nuestro proyecto tiene de nombre "app".</h3>
                <div className={styles.Image}><img src={Rails1} alt="" /></div>
                <h3>Para dar permisos a los grupos de usuario, damos click derecho al directorio y vamos a la opción "Propiedades".</h3>
                <div className={styles.Image}><img src={Rails2} alt="" /></div>
                <h3>Vamos a la pestaña "Compartir", y damos click en "Uso compartido avanzado".</h3>
                <div className={styles.Image}><img src={Rails3} alt="" /></div>
                <h3>Siguiendo los mismos pasos que usamos para Django, agregamos los Grupos 3 y 4.</h3>
                <div className={styles.Image}><img src={Rails4} alt="" /></div>
                <h3>Seleccionamos los permisos de "Cambiar" para el Grupo 3, y solo de "Leer" para el Grupo 4.</h3>
                <div className={styles.Image}><img src={Rails5} alt="" /></div>
                <h3>Una vez aceptados los cambios, nos aparecerá el icono de compartido en la carpeta.</h3>
                <div className={styles.Image}><img src={Rails6} alt="" /></div>
            </div>
        );
    }
}

export default Tarea10;