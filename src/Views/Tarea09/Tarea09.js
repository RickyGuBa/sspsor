import React, { Component } from 'react';
// Styles
import styles from "./Tarea09.module.css";
// Components
import TitleComponent from '../../Components/TitleComponent/TitleComponent';
// Images
import Windows1 from "./img/Windows1.png";
import Windows2 from "./img/Windows2.png";
import Windows3 from "./img/Windows3.png";
import Windows4 from "./img/Windows4.png";
import Windows5 from "./img/Windows5.png";
import Windows6 from "./img/Windows6.png";
import Windows7 from "./img/Windows7.png";
import Windows8 from "./img/Windows8.png";
import Windows9 from "./img/Windows9.png";

class Tarea09 extends Component {
    render() {
        return (
            <div className={styles.Tarea}>
                <TitleComponent>Instalar mínimo 10 usuarios y crear cuando menos 4 grupos de usuarios.</TitleComponent>
                <h2>Usuarios</h2>
                <h3>Abrimos el adminisrtador de servidor de Windows Server.</h3>
                <h3>Del panel del lado izquierto vamos a Configuración > Usuarios y grupos locales.</h3>
                <div className={styles.Image}><img src={Windows1} alt="" /></div>
                <h3>Entramos a la parte de Usuarios y damos click derecho para agregar un usuario nuevo.</h3>
                <div className={styles.Image}><img src={Windows2} alt="" /></div>
                <h3>Ahora llenamos los datos de usuario y establecemos la Configuración para la contraseña.</h3>
                <div className={styles.Image}><img src={Windows3} alt="" /></div>
                <h3>Repetimos el proceso hasta tener nuestros 10 usuarios creados.</h3>
                <div className={styles.Image}><img src={Windows4} alt="" /></div>
                <h2>Grupos</h2>
                <h3>Para los grupos regresamos a Usuarios y grupos locales, y entramos a la sección de Grupos.</h3>
                <h3>Damos click derecho para Grupo nuevo</h3>
                <div className={styles.Image}><img src={Windows5} alt="" /></div>
                <h3>Ingresamos nombre y descripción del grupo.</h3>
                <div className={styles.Image}><img src={Windows6} alt="" /></div>
                <h3>Para agregar miembros, damos click en el botón agregar.</h3>
                <h3>
                    En la nueva ventana tenemos que escribir el nombre de los usuarios con todo y la ruta mencionada, al finalizar damos 
                    click en Aceptar.
                </h3>
                <div className={styles.Image}><img src={Windows7} alt="" /></div>
                <h3>
                    Una vez seleccionados los usuarios, nos apareceran en la sección de miembros, y para crear el grupo damos click en 
                    crear.
                </h3>
                <div className={styles.Image}><img src={Windows8} alt="" /></div>
                <h3>Repetimos el proceso para crear nuestros 4 grupos de usuarios.</h3>
                <div className={styles.Image}><img src={Windows9} alt="" /></div>
            </div>
        );
    }
}

export default Tarea09;