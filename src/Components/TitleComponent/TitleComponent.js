import React from 'react';
// Styles
import styles from "./TitleComponent.module.css";

const TitleComponent = ({children}) => {
    return (
        <div className={styles.Title}>
            {children}
            <div className={styles.Line}/>
        </div>
    );
};

export default TitleComponent;