import React, { Component } from 'react';
import styles from "./SOSelector.module.css"

class SOSelector extends Component {

    getOSClassName = (OS) => {
        let res = styles.SelectOSItem
        if(OS === this.props.selected){
            res += " " + styles.SelectOSItemActive
        }
        return res
    }

    render() {
        return (
            <div className={styles.SelectOS}>
                {
                    this.props.systems.map((system, index) =>
                        <div className={this.getOSClassName(system.name)} onClick={() => this.props.onChange(system.name)} key={index}><system.icon />{system.name}</div>
                    )
                }
            </div>
        );
    }
}

export default SOSelector;