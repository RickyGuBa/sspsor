import React from 'react';
import { Route, Switch } from 'react-router-dom';
import './App.css';
// Views
import Home from './Views/Home/Home';
import Tareas from './Views/Tareas/Tareas';

function App() {
  return (
    <div className="App">
      <Switch>
        <Route exact path="/" component={Home} />
        <Route exact path="/tarea/:id" component={Tareas} />
      </Switch>
    </div>
  );
}

export default App;
